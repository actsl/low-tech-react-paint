import React from 'react';
import {createRoot} from 'react-dom/client';

class App extends React.Component {

	state = {started: false, canvas: null, context: null, stampId: "", lastColor: "black", 
		enableDraw: false};

	componentDidMount() {
		document.title = "low-tech-react-paint";
		document.body.style.width = "100%";
		document.body.style.margin = "0px";
		document.getElementById("black").style.border = "2px dashed silver";
		this.setState({canvas: document.getElementById("imageView")});
		this.setState({context: document.getElementById("imageView").getContext("2d")});
		this.setState((state, props) => {
			let c = state.context;
			c.fillStyle = "white";
			c.lineWidth = "5";
			c.fillRect(0, 0, state.canvas.width, state.canvas.height);
			return {context: c};
		});
	}

	endDraw = e => {
		this.setState({enableDraw: false});
		this.setState({started: false});
		this.state.context.closePath();
	}

	onMouseMove = e => {
		if (this.state.enableDraw && this.state.stampId.length < 1) {
			if (!this.state.started) {
				this.setState({started: true});
				this.state.context.beginPath();
				this.state.context.moveTo(e.nativeEvent.offsetX, e.nativeEvent.offsetY);
			} else
				this.state.context.lineTo(e.nativeEvent.offsetX, e.nativeEvent.offsetY);
			this.state.context.stroke();
		}
		document.getElementById("stats").innerHTML = e.nativeEvent.offsetX + ", " + 
			e.nativeEvent.offsetY;
	}

	onMouseUp = e => {
		if (e.button !== 0) return;
		this.endDraw(e);
		if (this.state.stampId.length > 0)
			this.state.context.drawImage(document.getElementById(this.state.stampId), 
				e.pageX - 90, e.pageY - 60, 80, 80);
	}

	onMouseDown = e => {
		if (e.button !== 0) return;
		this.setState({enableDraw: true})
	}

	onColor = e => {
		let c = this.state.context;
		c.strokeStyle = e.target.id;
		this.setState({context: c});
		document.getElementById(this.state.lastColor).style.border = "0px dashed silver";
		document.getElementById(e.target.id).style.border = "2px dashed silver";
		this.setState({lastColor: e.target.id});
		if (this.state.stampId.length > 0)
			document.getElementById(this.state.stampId).style.border = "0px dashed silver";
		this.setState({stampId: ""});
	}

	onFill = e => {
		document.getElementById(this.state.lastColor).style.border = "0px dashed silver";
		let c = this.state.context;
		c.fillStyle = this.state.context.strokeStyle;
		c.fillRect(0, 0, this.state.canvas.width, this.state.canvas.height);
		this.setState({context: c});
		if (this.state.stampId.length > 0)
			document.getElementById(this.state.stampId).style.border = "0px dashed silver";
		this.setState({stampId: ""});
	}

	onStamp = e => {
		document.getElementById(this.state.lastColor).style.border = "0px dashed silver";
		if (this.state.stampId.length > 0)
			document.getElementById(this.state.stampId).style.border = "0px dashed silver";
		this.setState({stampId: e.target.id});
		document.getElementById(e.target.id).style.border = "2px dashed silver";
	}

	render = () => (<>
		<div style={{paddingTop: "5px", float: "left"}}>
			<ColorPlace parentCallback={this.onColor} />
			<hr/>
			<div id="fill">
				<img src="fill.png" alt="" width="50" height="50" onClick={this.onFill} />
			</div>
			<StampPlace parentCallback={this.onStamp} />
		</div>
		<div style={{float: "left"}}>
			<canvas id="imageView" style={{border: "1px solid #000"}} width="1024" height="768"
				onMouseMove={this.onMouseMove} onMouseOut={this.endDraw} 
				onMouseDown={this.onMouseDown} onMouseUp={this.onMouseUp}>
				<p>Your browser is unsupported.</p>
			</canvas>		
		</div>
		<div style={{paddingRight: "1000px", float: "left"}}>
			<div id="stats" style={{fontSize: "16pt", paddingLeft: "20px", float: "left"}}>
				0 0
			</div>
			<div style={{fontSize: "16pt", paddingLeft: "50px", float: "left"}}>
				Made with React
			</div>
		</div>
	</>);
};

class ColorPlace extends React.Component {

	state = {colors: ["red", "pink", "fuchsia", "orange", "yellow", "lime", "green", "blue", 
		"purple", "black", "white"]};

	render = () => (
		<div id="colorPlace"> {
			this.state.colors.map(color => 
				<div id={color} key={color} style={{backgroundColor: color, width: "50px", 
					height: "47px"}} onClick={e => this.props.parentCallback(e)}>
				</div>
			)
		} </div>
	);

};

class StampPlace extends React.Component {

	state = {stamps: ["cat", "dog", "ladybug", "heart"]};

	render = () => (
		<div id="stampPlace"> {
			this.state.stamps.map(stamp => 
				<div id={stamp} key={stamp} onClick={e => this.props.parentCallback(e)}>
					<img id={stamp + "Img"} alt="" src={stamp + ".png"} 
						width="50" height="50" />
				</div>
			)
		} </div>
	);
};

const root = document.getElementById("root");
createRoot(root).render(<App />);

